package app

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import web.MyRoute

object PortalApp {

  implicit val system = ActorSystem("portal-component")
  implicit val materializer = ActorMaterializer()

  def main(args: Array[String]): Unit = {
    system.log.info("Starting web server on {}:{}", "0.0.0.0", 8080)
    Http().bindAndHandle(new MyRoute().route, "0.0.0.0", 8080)
  }
}
