package web

import java.net.InetAddress
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.server.Directives.{ complete, get, path, pathPrefix }
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.FileAndResourceDirectives
import akka.util.Timeout

import scala.util.Properties

class MyRoute(implicit val system: ActorSystem) extends FileAndResourceDirectives with EnableCORSDirectives with ReplaceDirective {

  val resourceDirectory = "assets"
  val replacements = Map(
    "/localhost:8444/" -> s"/${fromEnv("COMPONENT_URL", "localhost:8444")}/",
    "/localhost:8081/" -> s"/${fromEnv("ASSETSERVER_URL", "localhost:8081")}/",
    "$$HOSTNAME$$" -> InetAddress.getLocalHost.getHostName)

  implicit val timeout = Timeout(length = 500, TimeUnit.MILLISECONDS)

  lazy val assetRoute: Route =
    get {
      replace(replacements) {
        getFromResourceDirectory(resourceDirectory)
      }
    }

  val pingRoute: Route = {
    path("ping") {
      complete((OK, "pong"))
    }
  }

  val route: Route = {

    // format: OFF
    enableCORS {
      pathPrefix("portal") {
        assetRoute ~ pingRoute
      } ~
      pathEndOrSingleSlash {
        redirect("/portal/portal.html", StatusCodes.MovedPermanently)
      }
    }
    // format: ON
  }
}
